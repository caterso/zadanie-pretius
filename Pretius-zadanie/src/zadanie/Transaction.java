package zadanie;

import java.math.BigDecimal;

public class Transaction {
	private String name, surname, src_iban, dst_iban, currency;
	private BigDecimal amount;
	
	public Transaction(String name, String surname, String src_iban, String dst_iban, BigDecimal amount) {
		super();
		this.name = name;
		this.surname = surname;
		this.src_iban = src_iban;
		this.dst_iban = dst_iban;
		this.amount = amount;
	}
	
	public Transaction() {}
	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getSurname() {
		return surname;
	}
	public void setSurname(String surname) {
		this.surname = surname;
	}
	public String getSrcIban() {
		return src_iban;
	}
	public void setSrcIban(String src_iban) {
		this.src_iban = src_iban;
	}
	public String getDstIban() {
		return dst_iban;
	}
	public void setDstIban(String dst_iban) {
		this.dst_iban = dst_iban;
	}

	public BigDecimal getAmount() {
		return amount;
	}

	public void setAmount(BigDecimal amount) {
		this.amount = amount;
	}

	public void setCurrency(String currency) {
		this.currency = currency;
	}
	public String getCurrency() {
		return currency;
	}
	
	
	
}
