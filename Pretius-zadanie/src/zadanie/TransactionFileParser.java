package zadanie;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Scanner;

public class TransactionFileParser {	
	private File transactionsFile;
	public TransactionFileParser(String transactionsFileName) {		
		transactionsFile = new File(transactionsFileName);		
	}
	public ArrayList<Transaction> parse() throws IOException{
		ArrayList<Transaction> transactions = new ArrayList<>(80);
		try(BufferedReader fileReader = new BufferedReader(new FileReader(transactionsFile))) {
			String line = null;
			while ((line = fileReader.readLine()) != null) {
				if(line.isEmpty())
					continue;
				try (Scanner tokenizer = new Scanner(line)) {
					tokenizer.useDelimiter("@");
					Transaction transaction = new Transaction();
					while (tokenizer.hasNext()) {
						String token = tokenizer.next();
						switch (getName(token)) {
							case "name":
								transaction.setName(getValue(token));
								break;
							case "surname":
								transaction.setSurname(getValue(token));
								break;
							case "src_iban":
								transaction.setSrcIban(getValue(token));
								break;
							case "dst_iban":
								transaction.setDstIban(getValue(token));
								break;
							case "amount":
								String value = getValue(token);
								value = value.replace(",", ".");
								transaction.setCurrency(value.substring(value.length() - 3, value.length()));
								transaction.setAmount(new BigDecimal(value.replace(transaction.getCurrency(), "")));
						}
					}
					transactions.add(transaction);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		}catch (IOException e){
			throw e; // zapięte na try-with-resources w celu zamknięcia strumienia jeśli wystąpi błąd
			//tutaj niezamknięcie strumienia nas nie boli, ale w dużych apkach które działają bez przerwy, wiszące zasoby będą problemtyczne
			//rzucamy dalej żeby poinformować o błędzie
		}

		return transactions;
	}
	
	private String getName(String source){
		return source.split(":")[0];
	}
	
	private String getValue(String source){
		return source.split(":")[1];
	}
	
	
	
	public static void main(String[] args) {
		TransactionFileParser parser = new TransactionFileParser( args[0] );		
		try {
			ArrayList<Transaction> trans = parser.parse();
			BigDecimal decimal = new BigDecimal(0);
			for (Transaction transaction : trans) {
				decimal = decimal.add(transaction.getAmount());
			}
			System.out.println( "Suma "+ decimal.toString() + (trans.get(0)!=null?trans.get(0).getCurrency(): "PLN" ));
			
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

}
